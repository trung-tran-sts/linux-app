﻿using LinuxApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LinuxApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly DataContext _dbContext;

        public IndexModel(ILogger<IndexModel> logger,
            DataContext dbContext)
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public string Data { get; set; }

        public void OnGet()
        {
            Data = _dbContext.AppDatas.FirstOrDefault()?.Data ?? "Failed to load data";
        }
    }
}
